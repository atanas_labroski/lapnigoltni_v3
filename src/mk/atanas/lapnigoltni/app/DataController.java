package mk.atanas.lapnigoltni.app;

import android.app.Application;
import android.content.Context;
import mk.atanas.lapnigoltni.MainActivity;
import mk.atanas.lapnigoltni.NoInternetConnectionException;
import mk.atanas.lapnigoltni.model.LokalsRepository;
import mk.atanas.lapnigoltni.model.MunicipalitiesRepository;
import mk.atanas.lapnigoltni.model.Municipality;
import mk.atanas.lapnigoltni.utils.ConnectionDetector;

import java.util.List;

/**
 * Created by Atanas on 5/10/14.
 */
public class DataController {
    private Context context;
    private ConnectionDetector connectionDetector;
    public MunicipalitiesRepository municipalitiesRepository;
    public LokalsRepository lokalsRepository;

    private static DataController ourInstance = new DataController();

    public static DataController getInstance() {
        return ourInstance;
    }


    /**
     * The controller is set to private so we cannot instantiate this class
     */
    private DataController() {

    }

    public static void prepareDataController(Context context) {
        getInstance().context = context;
    }

    public boolean isConnected() {
        if(connectionDetector == null) {
            connectionDetector = new ConnectionDetector(context);
        }
        return connectionDetector.isConnectingToInternet();
    }

    public void fetchData() throws NoInternetConnectionException {
        if(!isConnected()) {
            throw new NoInternetConnectionException();
        }

        municipalitiesRepository = new MunicipalitiesRepository();
        lokalsRepository = new LokalsRepository();
        FetchData.fetchMunicipalities(municipalitiesRepository);
        FetchData.fetchLokals(lokalsRepository);


    }

}
