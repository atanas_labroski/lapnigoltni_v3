package mk.atanas.lapnigoltni.app;

import android.app.ProgressDialog;
import android.util.Log;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import mk.atanas.lapnigoltni.model.LokalsRepository;
import mk.atanas.lapnigoltni.model.MunicipalitiesRepository;
import mk.atanas.lapnigoltni.utils.Constants;
import org.json.JSONArray;

/**
 * Created by Atanas on 5/10/14.
 */
public class FetchData {
    private static final String TAG = FetchData.class.getSimpleName();

    public static void fetchMunicipalities(final MunicipalitiesRepository municipalitiesRepository) {
        JsonArrayRequest request = new JsonArrayRequest(Constants.URL_GET_MUNICIPALITIES,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        municipalitiesRepository.createFromJSONArray(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });

        // Adding request to request queue for municipalities
        AppController.getInstance().addToRequestQueue(request, Constants.URL_GET_MUNICIPALITIES_TAG);
    }


    public static void fetchLokals(final LokalsRepository lokalsRepository) {
        JsonArrayRequest request = new JsonArrayRequest(Constants.URL_GET_LOKALI,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        lokalsRepository.createFromJSONArray(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });

        // Adding request to request queue for lokals
        AppController.getInstance().addToRequestQueue(request, Constants.URL_GET_LOKALI_TAG);
    }
}
