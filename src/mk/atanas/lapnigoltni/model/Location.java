package mk.atanas.lapnigoltni.model;

import android.util.Log;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Atanas on 5/10/14.
 */
public class Location {
    private static final String TAG = Location.class.getSimpleName();

    private String deliveryType;
    private Municipality municipality;
    private List<Phone> phones;
    private String address;

    public Location(String deliveryType, Municipality municipality, String address) {
        this.deliveryType = deliveryType;
        this.municipality = municipality;
        this.address = address;
        this.phones = new ArrayList<Phone>();
    }

    public void addPhone(Phone phone) {
        if(phone.getPhone().equals(null)) {
            Log.i(TAG, "phone == null");
        }
        this.phones.add(phone);
    }
}
