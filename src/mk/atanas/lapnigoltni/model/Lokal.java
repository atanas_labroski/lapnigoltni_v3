package mk.atanas.lapnigoltni.model;

import java.util.ArrayList;

/**
 * Created by Atanas on 5/10/14.
 */
public class Lokal {
    private static final String TAG = Lokal.class.getSimpleName();

    private int ID;
    private String name;
    private String link;
    private String description;
    private String city;
    private String type;
    private String logoUrl;
    private ArrayList<DaysAndTime> workTime;
    private ArrayList<Municipality> municipalities;
    private ArrayList<Location> locations;

    public Lokal(int ID, String name, String link, String description, String city, String type, String logoUrl) {
        this.ID = ID;
        this.name = name;
        this.link = link;
        this.description = description;
        this.city = city;
        this.type = type;
        this.logoUrl = logoUrl;
        this.workTime = new ArrayList<DaysAndTime>();
        this.municipalities = new ArrayList<Municipality>();
        this.locations = new ArrayList<Location>();
    }

    public static String getTag() {
        return TAG;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public ArrayList<DaysAndTime> getWorkTime() {
        return workTime;
    }

    public void setWorkTime(ArrayList<DaysAndTime> workTime) {
        this.workTime = workTime;
    }

    public ArrayList<Municipality> getMunicipalities() {
        return municipalities;
    }

    public void setMunicipalities(ArrayList<Municipality> municipalities) {
        this.municipalities = municipalities;
    }

    public ArrayList<Location> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<Location> locations) {
        this.locations = locations;
    }

    @Override
    public String toString() {
        String string = "";
        string += "LokalID: " + this.getID() + "\n";
        string += "LokalName: " + this.getName() + "\n";
        return string;
    }
}
