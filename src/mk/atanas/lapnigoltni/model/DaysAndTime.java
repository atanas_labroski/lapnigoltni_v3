package mk.atanas.lapnigoltni.model;

/**
 * Created by Atanas on 5/10/14.
 */
public class DaysAndTime {
    private String day;
    private String time;

    public DaysAndTime(String day, String time) {
        this.day = day;
        this.time = time;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
