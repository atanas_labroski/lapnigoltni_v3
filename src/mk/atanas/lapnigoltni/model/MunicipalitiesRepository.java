package mk.atanas.lapnigoltni.model;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Atanas on 5/10/14.
 */
public class MunicipalitiesRepository {
    private static final String TAG = MunicipalitiesRepository.class.getSimpleName();
    private List<Municipality> municipalities;

    public static final String MunicipalityID = "opstinaid";
    public static final String MunicipalityName = "opstina";


    public MunicipalitiesRepository() {
        this.municipalities = new ArrayList<Municipality>();
    }

    public void addMunicipality(Municipality municipality) {
        this.municipalities.add(municipality);
    }

    public void clear() {
        municipalities.clear();
    }

    public Municipality findById(int id) {
        if (municipalities.size() != 0) {
            for (Municipality municipality : municipalities) {
                if (municipality.getID() == id) {
                    return municipality;
                }
            }
        }
        return null;
    }

    public Municipality findByName(String name) {
        if (municipalities.size() != 0) {
            for (Municipality municipality : municipalities) {
                if (municipality.getName().equals(name)) {

                    Log.i(TAG, "findByName success");
                    return municipality;
                }
            }
        }

        Log.i(TAG, "findByName failed : " + name);
        return null;
    }

    public void createFromJSONArray(JSONArray jsonArray) {
        if (jsonArray.length() == 0) {
            return;
        }

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject object = jsonArray.getJSONObject(i);
                Municipality municipality = new Municipality(object.getInt(MunicipalityID), object.getString(MunicipalityName));
                municipalities.add(municipality);
                Log.i(TAG, "Municipality add to list: id-" + municipality.getID() + ", name-" + municipality.getName() + ", current repository size: " + municipalities.size());
            } catch (JSONException ex) {
                ex.printStackTrace();
                Log.i(TAG, "error creating JSONObject from JSONArray");
            }
        }
    }
}
