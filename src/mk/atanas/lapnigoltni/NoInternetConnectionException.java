package mk.atanas.lapnigoltni;

import android.util.Log;

/**
 * Created by Atanas on 5/10/14.
 */
public class NoInternetConnectionException extends Exception {
    private static final String TAG = NoInternetConnectionException.class.getSimpleName();

    public NoInternetConnectionException() {
        Log.i(TAG, "constructor invoked");
    }
}
