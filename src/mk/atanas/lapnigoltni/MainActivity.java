package mk.atanas.lapnigoltni;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import mk.atanas.lapnigoltni.app.AppController;
import mk.atanas.lapnigoltni.app.DataController;
import mk.atanas.lapnigoltni.utils.Constants;
import org.json.JSONArray;

public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        try {
            DataController.getInstance().fetchData();
        } catch (NoInternetConnectionException e) {
            e.printStackTrace();
        }
    }
}
